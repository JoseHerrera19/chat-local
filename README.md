## PROYECTO CHAT LOCAL (BACKEND)  

Este proyecto tiene como finalidad ser un Chat local donde la comunicacion sea por diferentes navegadores o distintas pestañas para poder enviar mensajeentre de ellas, pidiendo inicialmente un nombre de usuario.  
 
Para la elaboración de este chat donde se utilizaron tecnologías (backend) como:  

*  Maven 
*  Java
*  Spring-boot
*  Websocket
  
Desarrollado con el IDE Eclipse y para el levantamiento del Servicio Laragon. 

#### Despliegue
1. Clonar nuestro proyecto a donde queramos guardarlo.
2. Abrir el proyecto como Maven Project en nuestro IDE de preferencia.
3. Ubicaremos archivos importantes:
* ChatApplication: El main de nuestro proyecto. Ubicado en el paquete com.exam.chat
*  WebSocketMensajeConfig: Este archivo contiene un atributo que ocuparemos en función de nuestro servicio (para este proceso se elegio Laragon) por lo que actualmente tiene la URL http://chat2.test/ en la línea 16. Este archivo ubicado en com.exam.chat.configuration 
* Application.properties: Donde se colocara el puerto correra nuestro proceso. En este caso se tiene el 8090. Este archivo se encuentra en src/main/resources. 
3. Una vez mencionado esto nos dirigimos al archivo ChatApplication y corremos nuestro proyecto. 
4. Ahora en nuestro navegador de nuestra preferencia nos dirigimos a la ruta: http://localhost:8090/socket lo cual nos debera mostrar en pantalla “Welcome to SockJS!”.



![alt text](https://snipboard.io/SANV79.jpg)

![alt text](https://snipboard.io/qjd1yz.jpg)

![alt text](https://snipboard.io/r3l741.jpg)

![alt text](https://snipboard.io/bt6DVa.jpg)



package com.exam.chat.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketMensajeConfig implements WebSocketMessageBrokerConfigurer {
	
	@Override
	public void registerStompEndpoints(final StompEndpointRegistry endpointRegistry) {
		endpointRegistry.addEndpoint("/socket")
        .setAllowedOrigins("http://chat2.test/")
        .withSockJS();
	}

	@Override
	public void configureMessageBroker(final MessageBrokerRegistry brokerRegistry) {
		brokerRegistry.setApplicationDestinationPrefixes("/app");
		brokerRegistry.enableSimpleBroker("/topic");
	}
	
}

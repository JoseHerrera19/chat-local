package com.exam.chat.controller;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;

import com.exam.chat.model.Chat;

@Controller
public class ChatController {

	@MessageMapping("/chat.send")
	@SendTo("/topic/public")
	
	public Chat enviarMensaje(@Payload final Chat chat) {
		System.out.println(chat.toString());
		return chat;	  
	}
	
	@MessageMapping("/chat.newUser")
	@SendTo("/topic/public")
	
	public Chat nuevoUsuario(@Payload final Chat chat , SimpMessageHeaderAccessor messageHeaderAccessor) {
		messageHeaderAccessor.getSessionAttributes().put("username", chat.getSender());
		System.out.println(chat.toString());
		return chat;
	}
	
}

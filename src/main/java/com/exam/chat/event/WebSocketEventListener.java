package com.exam.chat.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import com.exam.chat.model.Chat;
import com.exam.chat.model.enums.MessageType;

@Component
public class WebSocketEventListener {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(WebSocketEventListener.class);
	
	@Autowired
	private SimpMessageSendingOperations sendingOperations;
	
	@EventListener
	public void handleWebSockersConnectListener(final SessionConnectedEvent connectedEvent) {	
		LOGGER.info("Conectado");
	}
	
	@EventListener
	public void handleWebSockersDisconnectListener(final SessionDisconnectEvent disconnectEvent) {	
		final StompHeaderAccessor accessor = StompHeaderAccessor.wrap(disconnectEvent.getMessage());
		final String usuario = (String) accessor.getSessionAttributes().get("username");
		final Chat chat = Chat.builder().type(MessageType.DISCONNECT).sender(usuario).build();
		sendingOperations.convertAndSend("/topic/public", chat);
	}
	
	
}

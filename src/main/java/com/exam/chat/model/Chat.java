package com.exam.chat.model;

import com.exam.chat.model.enums.MessageType;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@ToString
public class Chat {
	
	@Getter
	private MessageType type;
	
	@Getter
	private String time;
	
	@Getter
	private String sender;
	
	@Getter
	private String content;
	
}

package com.exam.chat.model.enums;

public enum MessageType {
	CHAT, 
	CONNECT,
	DISCONNECT
}
